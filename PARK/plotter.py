import matplotlib.pyplot as plt
import matplotlib
from matplotlib.path import Path
import matplotlib.patches as patches
import json
import numpy as np
import pandas as pd
from matplotlib.pyplot import gca
from scipy.interpolate import make_interp_spline

def load_data(path):
    data = None
    with open(path) as file_content:
        data = json.load(file_content)
    return data

def convert_data(data):
    result = {}
    for k, v in data.items():
        for k2, v2 in v.items():
            if k2 in result:
                result[k2] = {**result[k2], **{k: v2}}
            else:
                result[k2] = {k: v2}
    return result
 
def main_park():
    path = "results/" + "park_anm_complete_run"
    data = load_data(path)

    factors2 = np.arange(1, 101, 1, dtype=np.float64)
    factors1 = np.arange(0.01, 1.01, 0.01)  # 1 / factors2

    combinations = []  # e.g., GAU, GAUxUNI, etc
    for k, _ in data["1.0"].items():
        combinations.append(k)
    
    values1 = {k: [] for k in combinations}
    values2 = {k: [] for k in combinations}
    
    for f in factors1:
        for k, v in data[str(np.round(f, 2))].items():
            values1[k].append([f, v])
            
    for f in factors2:
        for k, v in data[str(np.round(f, 2))].items():
            values2[k].append([f, v])
            
    values1_L = {}
    values1_NL = {}
    for k, v in values1.items():
        if k.startswith("NL"):
            values1_NL[k] = np.array(v)#.reshape(-1, 1)
        else:
            values1_L[k] = np.array(v)#.reshape(-1, 1)
            
    values2_L = {}
    values2_NL = {}
    for k, v in values2.items():
        if k.startswith("NL"):
            values2_NL[k] = np.array(v)#.reshape(-1, 1)
        else:
            values2_L[k] = np.array(v)#.reshape(-1, 1)
    
    park_create_plot("plots/park/1Linear", values1_L, np.arange(0, 1.1, 0.1), np.arange(0, 1.1, 0.1), False)
    park_create_plot("plots/park/1NonLinear", values1_NL, np.arange(0, 1.1, 0.1), np.arange(0, 1.1, 0.1), False)
    park_create_plot("plots/park/2Linear", values2_L, np.arange(0, 101, 10), np.arange(0, 1.1, 0.1))
    park_create_plot("plots/park/2NonLinear", values2_NL, np.arange(0, 101, 10), np.arange(0, 1.1, 0.1))
    
    
def park_create_plot(name, data, x_ticks, y_ticks, legend=True):
    color = ["#dd7e2a", "#2aa4dd", "#95dd2a", "#874edb", "#db4e6f", "#efed64", "#177c19", "#2830d3", "#ef64e4",
             "#332f2f", "#04cdf9"]
    ls = ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-", "-"]
    count = 0
    for k, v in data.items():
        a, b = np.array_split(v, 2, axis=1)
        #print(a)
        # Smoothing
        #X_Y_Spline = make_interp_spline(a.flatten(), b.flatten())
        #a = np.linspace(a.min(), a.max(), 500)
        #b = X_Y_Spline(a)

        #coefs = np.polyfit(a, b, 20)
        #b = np.polyval(coefs, a)

        plt.plot(a, b, color=color[count], label=k, alpha=0.75, linestyle=ls[count])
        count += 1
    plt.ylim(-0.05, 1.05)
    plt.axhline(0.5, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(1.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(0.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.xticks(x_ticks)
    plt.yticks(y_ticks)
    plt.grid(axis='both', alpha=0.3)
    plt.ylabel("Accuracy")
    plt.xlabel("Standard deviation factor for noise")
    if legend:
        plt.legend(
            bbox_to_anchor=(1, 1),
            loc="upper left",
        )
    plt.savefig(name + ".png", format="png", bbox_inches='tight', dpi=300)
    plt.clf()
    plt.cla()
    plt.close()
    



if __name__ == "__main__":
    main_park()
    
    
    
    
