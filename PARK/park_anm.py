import numpy as np
import json
import park



def main():
    mean = 0
    var = 1
    size = (1000, 1)

    ratios = np.concatenate([np.arange(0.01, 1.00, 0.01), np.arange(1, 101, 1)])

    final_result = {}

    regressor=None

    count = 1

    normal = np.random.normal
    uniform = np.random.uniform
    laplace = np.random.laplace

    for i in ratios:
        print("Loop: ", count, "/200")
        count += 1

        final_result[np.round(i, 2)] = {
            "GAU": test_distribution(mean, var, mean, var*i, size, normal, normal, linear=True, regressor=regressor),
            "UNI": test_distribution(mean - var, mean + var, mean - var*i, mean + var*i, size, uniform, uniform, linear=True, regressor=regressor),
            "LAP": test_distribution(mean, var, mean, var*i, size, laplace, laplace, linear=True, regressor=regressor),
            "NLGAU": test_distribution(mean, var, mean, var*i, size, normal, normal, linear=False, regressor=regressor),
            "NLUNI": test_distribution(mean - var, mean + var, mean - var*i, mean + var*i, size, uniform, uniform, linear=False, regressor=regressor),
            "NLLAP": test_distribution(mean, var, mean, var*i, size, laplace, laplace, linear=False, regressor=regressor),

            "GAUxUNI": test_distribution(mean, var, mean - var*i, mean + var*i, size, normal, uniform, linear=True, regressor=regressor),
            "GAUxLAP": test_distribution(mean, var, mean, var*i, size, normal, laplace, linear=True, regressor=regressor),
            "NL_GAUxUNI": test_distribution(mean, var, mean - var*i, mean + var*i, size, normal, uniform, linear=False, regressor=regressor),
            "NL_GAUxLAP": test_distribution(mean, var, mean, var*i, size, normal, laplace, linear=False, regressor=regressor),

            "UNIxGAU": test_distribution(mean - var, mean + var, mean, var*i, size, uniform, normal, linear=True, regressor=regressor),
            "UNIxLAP": test_distribution(mean - var, mean + var, mean, var*i, size, uniform, laplace, linear=True, regressor=regressor),
            "NL_UNIxGAU": test_distribution(mean - var, mean + var, mean, var*i, size, uniform, normal, linear=False, regressor=regressor),
            "NL_UNIxLAP": test_distribution(mean - var, mean + var, mean, var*i, size, uniform, laplace, linear=False, regressor=regressor),

            "LAPxGAU": test_distribution(mean, var, mean, var*i, size, laplace, normal, linear=True, regressor=regressor),
            "LAPxUNI": test_distribution(mean, var, mean - var*i, mean + var*i, size, laplace, uniform, linear=True, regressor=regressor),
            "NL_LAPxGAU": test_distribution(mean, var, mean, var*i, size, laplace, normal, linear=False, regressor=regressor),
            "NL_LAPxUNI": test_distribution(mean, var, mean - var*i, mean + var*i, size, laplace, uniform, linear=False, regressor=regressor),
        }

    f = open("results/park_anm_complete_run", "w")
    f.write(json.dumps(final_result))


"""
Modulation of test methods
"""
def test_distribution(x_mean, x_var, eps_mean, eps_var, size, x_dist, eps_dist, linear=True, regressor=None):

    success_count = 0

    for i in range(0, 100):
        x = x_dist(x_mean, x_var, size=size)
        eps = eps_dist(eps_mean, eps_var, size=size)
        if not linear:
            x = x**3
        y = x + eps

        _, b, _ = park.uncertainty_scoring([x, y])

        if len(b[0]) == 0 and len(b[1]) == 1 and b[1] == [0]:
            success_count += 1

    return success_count / 100

if __name__ == "__main__":
    main()
