import sklearn
from sklearn.linear_model import LinearRegression as LR
import ite
import stattests
import numpy as np
import json
from sklearn.linear_model import Ridge
from statsmodels.stats.diagnostic import het_breuschpagan
from statsmodels.stats.diagnostic import het_white
import statsmodels.api as sm
from scipy.sparse import linalg as sla
from scipy.stats import norm
from scipy.sparse import csc_matrix

from pycit import citest


def conditional_variance(x, y):
    r = LR()
    r.fit(x, y)
    y_res = r.predict(x) - y

    r.fit(x, y_res**2)
    y_pred = r.predict(x)
    return y_pred.mean()

def cov2corr(A):
    """
    covariance matrix to correlation matrix.
    """
    d = np.sqrt(A.diagonal())
    A = ((A.T/d).T)/d
    return A


def forward_stepwise_selection(x):
    s = np.arange(0, len(x), 1)
    x_nodes = dict(zip(s, x))
    ordering = []
    while len(s) > 1:
        print("")



def backward_stepwise_selection(x):
    s = np.arange(0, len(x), 1)
    m = s[-1::-1]
    x_nodes = dict(zip(s, x))
    ordering = []

    while len(s) > 1:  # for n in m:
        #print("s", s)
        #print("ordering", ordering)
        temp_cond_var = 0
        index = -1
        for j in s:
            s_j = s
            s_j = np.delete(s_j, np.where(s_j == j)[0][0])
            x_j = x_nodes[j]
            x_s = []
            for i in s_j:
                x_s.append(x_nodes[i])
            regressor = LR()
            x_s = np.concatenate(x_s, axis=1)
            cond_var = conditional_variance(x_s, x_j)
            if cond_var > temp_cond_var:
                temp_cond_var = cond_var
                index = j
        ordering.append(index)
        s = np.delete(s, np.where(s == index)[0][0])
    ordering.append(s[0])
    #print("s0", s[0])
    return x_nodes, ordering[-1::-1]


def uncertainty_scoring(x, alpha=0.05):
    # Step 1
    x_nodes, ordering = backward_stepwise_selection(x)

    n = len(x_nodes[0])
    #ordering = [2,0,1]

    #ordering = [2, 0, 1, 3, 4]

    indices = np.arange(0, len(x), 1)

    parents = {k: [] for k in indices}

    ind_test = ite.cost.BIHSIC_IChol()
    ind_test2 = ite.cost.BcondIShannon_HShannon()
    ind_test2 = ite.cost.BIHSIC_IChol()


    for i in range(1, len(ordering)):
        #covariance_matrix = np.cov([x.flatten(), y.flatten(), z.flatten()])
        #lu = sla.splu(covariance_matrix)
        #solved_matrix = lu.solve(np.identity(3))
        #rho = cov2corr(solved_matrix)
        #loop through rho to get last columns values until second last
        #Z_stat = np.abs( 1/2 * np.log( (1+Rho)/(1-Rho) ) * np.sqrt( n - 1 - m ) )
        #parent_pvalue = (1 - stats.norm.cdf(Z_stat)) * 2

        S = ordering[:i+1]

        # create first covariance matrix
        covariance_matrix = []
        for j in S:
            covariance_matrix.append(x_nodes[j].flatten())
        covariance_matrix = np.cov(covariance_matrix)
        #print("cov_matrxi", covariance_matrix)


        # create sparse matrix and solve it with identity matrix
        # thus we are calculating the inverse matrix
        covariance_matrix = csc_matrix(covariance_matrix, dtype=float)
        lu = sla.splu(covariance_matrix)
        solved_matrix = lu.solve(np.identity(i+1))
        #print("solved", solved_matrix)


        # get correlation values
        rho = cov2corr(solved_matrix)
        #print("rho1", rho)
        rho = rho[:, i][:i]
        #print("rho", rho)


        # calculate z scores
        Z_stat = np.abs(1 / 2 * np.log((1 + rho) / (1 - rho)) * np.sqrt(n - 1 - (i+1)))
        #print("z", Z_stat)


        # calculate p scores
        parent_pvalue = (1 - norm.cdf(Z_stat)) * 2
        #print("pvalues", parent_pvalue)


        # Finally add parents to parents dictionary
        for val in range(0, i):
            if parent_pvalue[val] < alpha:
                parents[ordering[i]].append(S[val])

    return x_nodes, parents, ordering


def cond_ind_test(y, x, cond_set):
    if len(cond_set) > 0:
        r1 = LR()
        r2 = LR()

        r1.fit(y, cond_set)
        r2.fit(x, cond_set)

        res1 = r1.predict(cond_set) - y
        res2 = r2.predict(cond_set) - x

        return np.corrcoef(res1.flatten(), res2.flatten())[0][1]
    else:
        return ite.cost.BIHSIC_IChol().estimation(np.concatenate([x, y], axis=1), [1, 1])  # stattests.hsic_gam(y, x)


def test_run():
    count = 0
    for i in range(0, 1000):
        w = np.random.normal(0, 1, (1000, 1))
        v = w + np.random.normal(0, 1, (1000, 1))
        a, b = uncertainty_scoring([w, v], alpha=0.1)
        if len(b[0]) == 0 and len(b[1]) == 1 and b[1][0] == 0:
            count += 1
    return count / 1000
    
def test(i):
    count = 0
    correct_order_count = 0
    for j in range(0, 1000):
        x = np.random.normal(0, 1, (1000, 1))
        y = x + np.random.normal(0, i, (1000, 1))
        _, b, c = uncertainty_scoring([x, y])
        if b == {0: [], 1: [0]}:
            count += 1
        if c == [0, 1]:
            correct_order_count += 1
    return count, correct_order_count

