function entry()
    tic;
    main();
    toc
end



function [b, a] = linear_regression(x, y)
    r = mean(((x - mean(x)) / std(x,1)) .* ((y - mean(y)) / std(y,1)));
    b = r * std(y,1) / std(x,1);
    a = mean(y) - b*mean(x);
end



function [res1, res2] = inference(x, y, E)
    [x_test, y_test, y_res, x_res] = decoupled_regression_step(x, y, 0.8);
    [y_res_2, x_res_2] = coupled_regression_step(x, y);

    res1 = decoupled_inference(x_test, y_res, y_test, x_res, E);
    res2 = coupled_inference(x, y_res_2, y, x_res_2, E);
end

function [y_res_2, x_res_2] = coupled_regression_step(x, y)
    [b, a] = linear_regression(x, y);
    y_res_2 = b*x + a - y;

    [b, a] = linear_regression(y, x);
    x_res_2 = b*y + a - x;
end

function [x_test, y_test, y_res, x_res] = decoupled_regression_step(x, y, split)
    x_train = x(1:fix(length(x)*split));
    x_test = x(fix(length(x)*split + 1:length(x)));
    y_train = y(1:fix(length(y)*split));
    y_test = y(fix(length(y)*split + 1:length(y)));

    [b, a] = linear_regression(x_train, y_train);
    y_res = b*x_test + a - y_test;

    [b, a] = linear_regression(y_train, x_train);
    x_res = b*y_test + a - x_test;
end


function res1 = decoupled_inference(x, y_res, y, x_res, E)
    data1 = vertcat(x, y_res);
    data2 = vertcat(y, x_res);
    ds = [1,1];

    res1 = containers.Map;
    temp = containers.Map;

    [testStat1, tresh1] = hsicTestGamma(reshape(x, [200,1]), reshape(y_res, [200,1]), 0.05, E("p"));
    [testStat2, tresh2] = hsicTestGamma(reshape(y, [200,1]), reshape(x_res, [200,1]), 0.05, E("p"));

    temp("X->Y") = testStat1 < tresh1;
    temp("Y->X") = testStat2 < tresh2;
    % the reshape size is only defined locally and is 1000 * 0.8 (split) = 200
    res1("HSIC_S") = temp;
end



function res2 = coupled_inference(x, y_res, y, x_res, E)
    data1 = vertcat(x, y_res);
    data2 = vertcat(y, x_res);
    ds = [1,1];

    res2 = containers.Map;
    temp = containers.Map;

    [testStat1, tresh1] = hsicTestGamma(reshape(x, [1000,1]), reshape(y_res, [1000,1]), 0.05, E("p"));
    [testStat2, tresh2] = hsicTestGamma(reshape(y, [1000,1]), reshape(x_res, [1000,1]), 0.05, E("p"));

    temp("X->Y") = testStat1 < tresh1;
    temp("Y->X") = testStat2 < tresh2;
 

    % the reshape size is only defined locally.
    res2("HSIC") = temp;
end


function dist_res = test_distribution(x_offset, x_sigma, eps_offset, eps_i_factor, x_dist, eps_dist, linear, E)
    success_count_hsic = 0;
    success_count_hsic_s = 0;

    for test = 1:100
        x = x_dist(1, 1000, x_offset, x_sigma); %x_offset != 0 if uniform noise
        eps = eps_dist(1, 1000, eps_offset, eps_i_factor);
        if linear == 0
            x = x.^3;
        end
        y = x + eps;
        
        [res1, res2] = inference(x, y, E);

        HSIC = res2('HSIC');
        HSIC_S = res1('HSIC_S');

        if HSIC("X->Y") == 1 && HSIC("Y->X") == 0
            success_count_hsic = success_count_hsic + 1;
        end
        if HSIC_S("X->Y") == 1 && HSIC_S("Y->X") == 0
            success_count_hsic_s = success_count_hsic_s + 1;
        end
    end

    dist_res = containers.Map;

    dist_res('HSIC') = success_count_hsic / 100;
    dist_res('HSIC_S') = success_count_hsic_s / 100;
end






function main()
    % Map of Estimators
    E = containers.Map;

    E("sh_knn") = HShannon_kNN_k_initialization(1);

    sh_knn_2 = HShannon_kNN_k_initialization(1);
    sh_knn_2.kNNmethod = 'knnsearch';
    sh_knn_2.NSmethod = 'kdtree';
    sh_knn_2.epsi = 0.1;
    E("sh_knn_2") = sh_knn_2;

    sh_knn_3 = HShannon_kNN_k_initialization(1);
    sh_knn_3.k = 5;
    E("sh_knn_3") = sh_knn_3;

    E("sh_spacing_v") = HShannon_spacing_V_initialization(1);
    E("sh_maxent1") = HShannon_MaxEnt1_initialization(1);
    E("sh_maxent2") = HShannon_MaxEnt2_initialization(1);

    E("hsic_ic") = IHSIC_initialization(1);

    hsic_ic2 = IHSIC_initialization(1);
    hsic_ic2.eta = 0.01;
    E("hsic_ic2") = hsic_ic2;

    E("distcorr") = IdCor_initialization(1);
    E("distcov") = IdCov_initialization(1);
    E("hoeffding") = IHoeffding_initialization(1);

    p.sigx = -1;
    p.sigy = -1;

    E("p") = p;
    
    f1 = 0.01:0.01:0.99;
    f2 = 1:1:100;
    
    F = [f1,f2];

    % F = [1];

    final_res = containers.Map;
    
    normal = @normal_rng;
    uniform = @uniform_rng;
    laplace = @laprnd;
        
    for i = F
        fprintf("Loop: %d\n", i);
        temp_res = containers.Map;
        % test_distribution(x_offset, x_sigma, eps_offset, eps_i_factor, x_dist, eps_dist, linear)
        temp_res("GAU")        = test_distribution(0, 1, 0, i,   normal, normal, 1, E);
        temp_res("UNI")        = test_distribution(1, 2, i, 2*i, uniform, uniform, 1, E);
        temp_res("LAP")        = test_distribution(0, 1, 0, i,   laplace, laplace, 1, E);
        temp_res("NLGAU")      = test_distribution(0, 1, 0, i,   normal, normal, 0, E);
        temp_res("NLUNI")      = test_distribution(1, 2, i, 2*i, uniform, uniform, 0, E);
        temp_res("NLLAP")      = test_distribution(0, 1, 0, i,   laplace, laplace, 0, E);
        temp_res("GAUxUNI")    = test_distribution(0, 1, i, 2*i, normal, uniform, 1, E);
        temp_res("GAUxLAP")    = test_distribution(0, 1, 0, i,   normal, laplace, 1, E);
        temp_res("NL_GAUxUNI") = test_distribution(0, 1, i, 2*i, normal, uniform, 0, E);
        temp_res("NL_GAUxLAP") = test_distribution(0, 1, 0, i,   normal, laplace, 0, E);
        temp_res("UNIxGAU")    = test_distribution(1, 2, 0, i,   uniform, normal, 1, E);
        temp_res("UNIxLAP")    = test_distribution(1, 2, 0, i,   uniform, laplace, 1, E);
        temp_res("NL_UNIxGAU") = test_distribution(1, 2, 0, i,   uniform, normal, 0, E);
        temp_res("NL_UNIxLAP") = test_distribution(1, 2, 0, i,   uniform, laplace, 0, E);
        temp_res("LAPxGAU")    = test_distribution(0, 1, 0, i,   laplace, normal, 1, E);
        temp_res("LAPxUNI")    = test_distribution(0, 1, i, 2*i, laplace, uniform, 1, E);
        temp_res("NL_LAPxGAU") = test_distribution(0, 1, 0, i,   laplace, normal, 0, E);
        temp_res("NL_LAPxUNI") = test_distribution(0, 1, i, 2*i, laplace, uniform, 0, E);
        
        final_res(string(i)) = temp_res;
    end

    fileID = fopen("Resit2CompleteRun", "w");
    fprintf(fileID, jsonencode(final_res));
end







function y  = normal_rng(m, n, offset, sigma)
   y = sigma .* randn(m, n) - offset;
end




function y = uniform_rng(m, n, offset, sigma)
   y = sigma .* rand(m, n) - offset;
end



function y  = laprnd(m, n, mu, sigma)
    %LAPRND generate i.i.d. laplacian random number drawn from laplacian distribution
    %   with mean mu and standard deviation sigma. 
    %   mu      : mean
    %   sigma   : standard deviation
    %   [m, n]  : the dimension of y.
    %   Default mu = 0, sigma = 1. 
    %   For more information, refer to
    %   http://en.wikipedia.org./wiki/Laplace_distribution
    %   Author  : Elvis Chen (bee33@sjtu.edu.cn)
    %   Date    : 01/19/07
    %Check inputs
    if nargin < 2
        error('At least two inputs are required');
    end
    if nargin == 2
        mu = 0; sigma = 1;
    end
    if nargin == 3
        sigma = 1;
    end
    % Generate Laplacian noise
    u = rand(m, n)-0.5;
    b = sigma / sqrt(2);
    y = mu - b * sign(u).* log(1- 2* abs(u));
end



function [H]=rbf_dot(patterns1,patterns2,deg);
    %Note : patterns are transposed for compatibility with C code.
    size1=size(patterns1);
    size2=size(patterns2);
    %new vectorised version
    G = sum((patterns1.*patterns1),2);
    H = sum((patterns2.*patterns2),2);

    Q = repmat(G,1,size2(1));
    R = repmat(H',size1(1),1);

    H = Q + R - 2*patterns1*patterns2';


    H=exp(-H/2/deg^2);
end

function [testStat, thresh] = hsicTestGamma(X,Y,alpha,params);
    % Source: https://github.com/amber0309/HSIC

    m=size(X,1);
    %Set kernel size to median distance between points, if no kernel specified.
    %Use at most 100 points (since median is only a heuristic, and 100 points
    %is sufficient for a robust estimate).
    if params.sigx == -1
        size1=size(X,1);
        if size1>100
          Xmed = X(1:100,:);
          size1 = 100;
        else
          Xmed = X;
        end
        G = sum((Xmed.*Xmed),2);
        Q = repmat(G,1,size1);
        R = repmat(G',size1,1);
        dists = Q + R - 2*Xmed*Xmed';
        dists = dists-tril(dists);
        dists=reshape(dists,size1^2,1);
        params.sigx = sqrt(0.5*median(dists(dists>0)));  %rbf_dot has factor of two in kernel
    end

    if params.sigy == -1
        size1=size(Y,1);
        if size1>100
          Ymed = Y(1:100,:);
          size1 = 100;
        else
          Ymed = Y;
        end    
        G = sum((Ymed.*Ymed),2);
        Q = repmat(G,1,size1);
        R = repmat(G',size1,1);
        dists = Q + R - 2*Ymed*Ymed';
        dists = dists-tril(dists);
        dists=reshape(dists,size1^2,1);
        params.sigy = sqrt(0.5*median(dists(dists>0)));
    end



    bone = ones(m,1);
    H = eye(m)-1/m*ones(m,m);


    K = rbf_dot(X,X,params.sigx);
    L = rbf_dot(Y,Y,params.sigy);

    Kc = H*K*H; %Note: these are slightly biased estimates of centred Gram matrices
    Lc = H*L*H;
      
    %NOTE: we fit Gamma to testStat*m
    testStat = 1/m * sum(sum(Kc'.*Lc));    %%%% TEST STATISTIC: m*HSICb (under H1)
      
    varHSIC = (1/6 * Kc.*Lc).^2;
      
    varHSIC = 1/m/(m-1)* (  sum(sum(varHSIC)) - sum(diag(varHSIC))  ); 
    %second subtracted term is bias correction
      
    varHSIC = 72*(m-4)*(m-5)/m/(m-1)/(m-2)/(m-3)  *  varHSIC;  %variance under H0


    K = K-diag(diag(K));
    L = L-diag(diag(L));

    muX = 1/m/(m-1)*bone'*(K*bone);
    muY = 1/m/(m-1)*bone'*(L*bone);
      
    mHSIC  = 1/m * ( 1 +muX*muY  - muX - muY )         ;  %mean under H0


    al = mHSIC^2 / varHSIC;
    bet = varHSIC*m / mHSIC;   %NOTE: threshold for hsicArr*m

    thresh = icdf('gam',1-alpha,al,bet);    %%%% TEST THRESHOLD
end




