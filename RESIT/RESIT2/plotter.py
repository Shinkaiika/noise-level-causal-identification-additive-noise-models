import matplotlib.pyplot as plt
import matplotlib
from matplotlib.path import Path
import matplotlib.patches as patches
import json
import numpy as np
import pandas as pd
from matplotlib.pyplot import gca
from scipy.interpolate import make_interp_spline

def load_data(path):
    data = None
    with open(path) as file_content:
        data = json.load(file_content)
    return data

def convert_data(data):
    result = {}
    for k, v in data.items():
        for k2, v2 in v.items():
            if k2 in result:
                result[k2] = {**result[k2], **{k: v2}}
            else:
                result[k2] = {k: v2}
    return result


def create_plot2(name, data, x_ticks, y_ticks, legend=True):
    color = ["#dd7e2a", "#2aa4dd", "#95dd2a", "#1c5621", "#874edb", "#db4e6f", 
            "#efed64", "#177c19", "#2830d3", "#ef64e4",  "#332f2f", "#04cdf9"]
    ls = ["-", "-", "-", "-", "-" ,"-", "--", "--", "--", "--", "--", "--"]
    count = 0
    for k, v in data.items():
        a, b = np.array_split(v, 2, axis=1)
        #print(a)
        # Smoothing
        #X_Y_Spline = make_interp_spline(a.flatten(), b.flatten())
        #a = np.linspace(a.min(), a.max(), 500)
        #b = X_Y_Spline(a)

        #coefs = np.polyfit(a, b, 20)
        #b = np.polyval(coefs, a)
        
        if k.startswith("NL"):
            ls = "--"
        else:
            ls = "-"

        plt.plot(a, b, color=color[count], label=k, alpha=0.75, linestyle=ls)
        count += 1
    plt.ylim(-0.05, 1.05)
    plt.axhline(0.5, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(1.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(0.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.xticks(x_ticks)
    plt.yticks(y_ticks)
    plt.grid(axis='both', alpha=0.3)
    plt.ylabel("Accuracy")
    plt.xlabel("Standard deviation factor for noise")
    if legend:
        plt.legend(
            bbox_to_anchor=(1, 1),
            loc="upper left",
        )
    plt.savefig(name + ".png", format="png", bbox_inches='tight', dpi=300)
    plt.clf()
    plt.cla()
    plt.close()

def main2():
    path = "results/" + "Resit2CompleteRun"
    data = load_data(path)

    factors2 = np.arange(1, 101, 1)
    factors1 = np.arange(0.01, 1.00, 0.01)
    np.append(factors1, 1)# 1 / factors2

    combinations = []  # e.g., GAU, GAUxUNI, etc
    for k, _ in data["1"].items():
        combinations.append(k)
        
    combinations = [
        ['GAU', 'UNI', 'LAP', 'NLGAU', 'NLUNI', 'NLLAP'],
        ['GAUxUNI', 'GAUxLAP', 'NL_GAUxUNI', 'NL_GAUxLAP'],
        ['UNIxGAU', 'UNIxLAP', 'NL_UNIxGAU', 'NL_UNIxLAP'],
        ['LAPxGAU', 'LAPxUNI', 'NL_LAPxGAU', 'NL_LAPxUNI'],
    ]

    # np.array_split(z, 2, axis=1)
    
    count = 0

    for comb in combinations:
        plot_data = {}
        for j in comb:
            temp_list = []
            for i in factors1:
                temp = data[str(np.round(i, 2))]
                temp_list.append([i, temp[j]["HSIC"]])
            plot_data[j.replace("x","+")] = np.array(temp_list)


        create_plot2("plots/1_HSIC_" + str(count), plot_data, np.arange(0, 1.1, 0.1), np.arange(0, 1.1, 0.1), legend=False)
        #continue

        plot_data = {}
        for j in comb:
            temp_list = [] 
            for i in factors2:
                temp = data[str(np.round(i, 2))]
                temp_list.append([i, temp[j]["HSIC"]])
            plot_data[j.replace("x","+")] = np.array(temp_list)

        create_plot2("plots/2_HSIC_" + str(count), plot_data, np.arange(0, 101, 10), np.arange(0, 1.1, 0.1))
        


        plot_data = {}
        for j in comb:
            temp_list = [] 
            for i in factors1:
                temp = data[str(np.round(i, 2))]
                temp_list.append([i, temp[j]["HSIC_S"]])
            plot_data[j.replace("x","+")] = np.array(temp_list)


        create_plot2("plots/1_HSIC_S_" + str(count), plot_data, np.arange(0, 1.1, 0.1), np.arange(0, 1.1, 0.1), legend=False)
        #continue

        plot_data = {}
        for j in comb:
            temp_list = [] 
            for i in factors2:
                temp = data[str(np.round(i, 2))]
                temp_list.append([i, temp[j]["HSIC_S"]])
            plot_data[j.replace("x","+")] = np.array(temp_list)

        create_plot2("plots/2_HSIC_S_" + str(count), plot_data, np.arange(0, 101, 10), np.arange(0, 1.1, 0.1))
 



        count += 1





if __name__ == "__main__":
    main2()
    
    
    
    
