import matplotlib.pyplot as plt
import matplotlib
from matplotlib.path import Path
import matplotlib.patches as patches
import json
import numpy as np
import pandas as pd
from matplotlib.pyplot import gca
from scipy.interpolate import make_interp_spline
from matplotlib import cm

def load_data(path):
    data = None
    with open(path) as file_content:
        data = json.load(file_content)
    return data

def convert_data(data):
    result = {}
    for k, v in data.items():
        for k2, v2 in v.items():
            if k2 in result:
                result[k2] = {**result[k2], **{k: v2}}
            else:
                result[k2] = {k: v2}
    return result
    
   

def compressed_plot(name, data, x_ticks, y_ticks, legend=True):
    color = ["#dd7e2a", "#2aa4dd", "#95dd2a", "#1c5621", "#874edb", "#db4e6f", "#efed64", "#177c19", "#2830d3", "#ef64e4",
             "#332f2f", "#04cdf9"]
    ls = ["-", "-", "-", "-", "-", "-", "--", "--", "--", "--", "--", "--"]
    count = 0
    
    for k, v in data.items():
        a, b = np.array_split(v, 2, axis=1)
        #print(a)
        # Smoothing
        #X_Y_Spline = make_interp_spline(a.flatten(), b.flatten())
        #a = np.linspace(a.min(), a.max(), 500)
        #b = X_Y_Spline(a)

        #coefs = np.polyfit(a, b, 20)
        #b = np.polyval(coefs, a)

        plt.plot(a, b, color=color[count], label=k, alpha=0.75, linestyle=ls[count])
        count += 1
    plt.ylim(-0.05, 1.05)
    plt.axhline(0.5, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(1.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(0.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.xticks(np.arange(0, 441, 1), x_ticks, rotation="vertical")
    #plt.axes().set_xticks(np.arange(0, 441, 1))
    #plt.axes().set_xticklabels(x_ticks, rotation="vertical")
    
    for i in range(0, len(x_ticks)):
        if i != 0 and i != 440 and i != 210 and i != 230:
            x_ticks[i] = ""
    
    
    plt.xticks(np.arange(0, len(x_ticks), 1), x_ticks, rotation="vertical")
    plt.yticks(y_ticks)
    plt.grid(axis='both', alpha=0.3)
    plt.ylabel("Accuracy")
    plt.xlabel(r"$\mu_X / \mu_N$")
    if legend:
        plt.legend(
            bbox_to_anchor=(1, 1),
            loc="upper left",
        )
    
    #plt.figure().set_figwidth(10)
    #plt.axes([0, 0, 10, 10])
    plt.savefig(name + "com.png", format="png", bbox_inches='tight', dpi=300)
    plt.clf()
    plt.cla()
    plt.close()
    
    
def main2():
    path = "results/" + "Resit3CompleteRun"
    data = load_data(path)

    factors2 = np.arange(1, 101, 1)
    factors1 = np.arange(0.01, 1.00, 0.01)
    np.append(factors1, 1)

    combinations = []  # e.g., GAU, GAUxUNI, etc
    for k, _ in data["0/0"].items():
        combinations.append(k)
        
    
    _means = np.arange(-100, 110, 10)
    
    scenarios = []
    for i in _means:
        for j in _means:
            scenarios.append(str(i) + "/" + str(j))
            
    x_coords = np.arange(0, 442, 1)
    
    d = dict(zip(x_coords, scenarios))

    # np.array_split(z, 2, axis=1)

    for comb in combinations:
        if comb != "GAUxUNI" and comb != "NL_UNIxGAU" and comb != "NLUNI" and comb != "UNIxLAP":
            continue
        if comb == "GAUxUNI" or comb == "NLUNI":
            LEGEND = False
        else:
            LEGEND = True
        HSIC, HSIC_IC, HSIC_IC2, DISTCOV, DISTCORR, HOEFFDING, SH_KNN, SH_KNN_2, SH_KNN_3, SH_MAXENT1, SH_MAXENT2, SH_SPACING_V = [], [], [], [], [], [], [], [], [], [], [], []
        for k, v in d.items():
            temp = data[v]
            HSIC.append([k, temp[comb]["HSIC_S"]])
            HSIC_IC.append([k, temp[comb]["HSIC_IC_S"]])
            HSIC_IC2.append([k, temp[comb]["HSIC_IC2_S"]])
            DISTCOV.append([k, temp[comb]["DISTCOV_S"]])
            DISTCORR.append([k, temp[comb]["DISTCORR_S"]])
            HOEFFDING.append([k, temp[comb]["HOEFFDING_S"]])
            SH_KNN.append([k, temp[comb]["SH_KNN_S"]])
            SH_KNN_2.append([k, temp[comb]["SH_KNN_2_S"]])
            SH_KNN_3.append([k, temp[comb]["SH_KNN_3_S"]])
            SH_MAXENT1.append([k, temp[comb]["SH_MAXENT1_S"]])
            SH_MAXENT2.append([k, temp[comb]["SH_MAXENT2_S"]])
            SH_SPACING_V.append([k, temp[comb]["SH_SPACING_V_S"]])

        #print(HSIC)
        #print(np.array(HSIC))

        plot_data = {
            'HSIC': np.array(HSIC),
            'HSIC_IC': np.array(HSIC_IC),
            'HSIC_IC2': np.array(HSIC_IC2),
            'DISTCOV': np.array(DISTCOV),
            'DISTCORR': np.array(DISTCORR),
            'HOEFFDING': np.array(HOEFFDING),
            'SH_KNN': np.array(SH_KNN),
            'SH_KNN_2': np.array(SH_KNN_2),
            'SH_KNN_3': np.array(SH_KNN_3),
            'SH_MAXENT1': np.array(SH_MAXENT1),
            'SH_MAXENT2': np.array(SH_MAXENT2),
            'SH_SPACING_V': np.array(SH_SPACING_V),
        }
                                                 #np.arange(0, 441, 1)
        compressed_plot("plots/1" + comb, plot_data, scenarios, np.arange(0, 1.1, 0.1), legend=LEGEND)
        


if __name__ == "__main__":
    main2()
    
    
    
    
