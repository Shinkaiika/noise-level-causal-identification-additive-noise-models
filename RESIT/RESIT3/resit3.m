function entry()
    tic;
    main();
    toc
end



function [b, a] = linear_regression(x, y)
    r = mean(((x - mean(x)) / std(x,1)) .* ((y - mean(y)) / std(y,1)));
    b = r * std(y,1) / std(x,1);
    a = mean(y) - b*mean(x);
end



function [res1, res2] = inference(x, y, E)
    [x_test, y_test, y_res, x_res] = decoupled_regression_step(x, y, 0.8);
    [y_res_2, x_res_2] = coupled_regression_step(x, y);

    res1 = decoupled_inference(x_test, y_res, y_test, x_res, E);
    res2 = coupled_inference(x, y_res_2, y, x_res_2, E);
end

function [y_res_2, x_res_2] = coupled_regression_step(x, y)
    [b, a] = linear_regression(x, y);
    y_res_2 = b*x + a - y;

    [b, a] = linear_regression(y, x);
    x_res_2 = b*y + a - x;
end

function [x_test, y_test, y_res, x_res] = decoupled_regression_step(x, y, split)
    x_train = x(1:fix(length(x)*split));
    x_test = x(fix(length(x)*split + 1:length(x)));
    y_train = y(1:fix(length(y)*split));
    y_test = y(fix(length(y)*split + 1:length(y)));

    [b, a] = linear_regression(x_train, y_train);
    y_res = b*x_test + a - y_test;

    [b, a] = linear_regression(y_train, x_train);
    x_res = b*y_test + a - x_test;
end


function res1 = decoupled_inference(x, y_res, y, x_res, E)
    data1 = vertcat(x, y_res);
    data2 = vertcat(y, x_res);
    ds = [1,1];

    res1 = containers.Map;
 
    % the reshape size is only defined locally and is 1000 * 0.8 (split) = 200
    res1("HSIC_S") = hsicTestGamma(reshape(x, [200,1]), reshape(y_res, [200,1]), 0.05, E("p")) < hsicTestGamma(reshape(y, [200,1]), reshape(x_res, [200,1]), 0.05, E("p"));
    res1("HSIC_IC_S") = IHSIC_estimation(data1, [1,1], E("hsic_ic")) < IHSIC_estimation(data2, [1,1], E("hsic_ic"));
    res1("HSIC_IC2_S") = IHSIC_estimation(data1, [1,1], E("hsic_ic2")) < IHSIC_estimation(data2, [1,1], E("hsic_ic2"));
    res1("DISTCOV_S") = IdCov_estimation(data1, [1,1], E("distcov")) < IdCov_estimation(data2, [1,1], E("distcov"));
    res1("DISTCORR_S") = IdCor_estimation(data1, [1,1], E("distcorr")) < IdCor_estimation(data2, [1,1], E("distcorr"));
    res1("HOEFFDING_S") = IHoeffding_estimation(data1, [1,1], E("hoeffding")) < IHoeffding_estimation(data2, [1,1], E("hoeffding"));
    
    res1("SH_KNN_S") = HShannon_kNN_k_estimation(x, E("sh_knn")) + HShannon_kNN_k_estimation(y_res, E("sh_knn")) < HShannon_kNN_k_estimation(y, E("sh_knn")) + HShannon_kNN_k_estimation(x_res, E("sh_knn"));
    res1("SH_KNN_2_S") = HShannon_kNN_k_estimation(x, E("sh_knn_2")) + HShannon_kNN_k_estimation(y_res, E("sh_knn_2")) < HShannon_kNN_k_estimation(y, E("sh_knn_2")) + HShannon_kNN_k_estimation(x_res, E("sh_knn_2"));
    res1("SH_KNN_3_S") = HShannon_kNN_k_estimation(x, E("sh_knn_3")) + HShannon_kNN_k_estimation(y_res, E("sh_knn_3")) < HShannon_kNN_k_estimation(y, E("sh_knn_3")) + HShannon_kNN_k_estimation(x_res, E("sh_knn_3"));
    res1("SH_SPACING_V_S") = HShannon_spacing_V_estimation(x, E("sh_spacing_v")) + HShannon_spacing_V_estimation(y_res, E("sh_spacing_v")) < HShannon_spacing_V_estimation(y, E("sh_spacing_v")) + HShannon_spacing_V_estimation(x_res, E("sh_spacing_v"));
    res1("SH_MAXENT1_S") = HShannon_MaxEnt1_estimation(x, E("sh_maxent1")) + HShannon_MaxEnt1_estimation(y_res, E("sh_maxent1")) < HShannon_MaxEnt1_estimation(y, E("sh_maxent1")) + HShannon_MaxEnt1_estimation(x_res, E("sh_maxent1"));
    res1("SH_MAXENT2_S") = HShannon_MaxEnt2_estimation(x, E("sh_maxent2")) + HShannon_MaxEnt2_estimation(y_res, E("sh_maxent2")) < HShannon_MaxEnt2_estimation(y, E("sh_maxent2")) + HShannon_MaxEnt2_estimation(x_res, E("sh_maxent2"));
end



function res2 = coupled_inference(x, y_res, y, x_res, E)
    data1 = vertcat(x, y_res);
    data2 = vertcat(y, x_res);
    ds = [1,1];

    res2 = containers.Map;


    % the reshape size is only defined locally.
    res2("HSIC") = hsicTestGamma(reshape(x, [1000,1]), reshape(y_res, [1000,1]), 0.05, E("p")) < hsicTestGamma(reshape(y, [1000,1]), reshape(x_res, [1000,1]), 0.05, E("p"));
    res2("HSIC_IC") = IHSIC_estimation(data1, [1,1], E("hsic_ic")) < IHSIC_estimation(data2, [1,1], E("hsic_ic"));
    res2("HSIC_IC2") = IHSIC_estimation(data1, [1,1], E("hsic_ic2")) < IHSIC_estimation(data2, [1,1], E("hsic_ic2"));
    res2("DISTCOV") = IdCov_estimation(data1, [1,1], E("distcov")) < IdCov_estimation(data2, [1,1], E("distcov"));
    res2("DISTCORR") = IdCor_estimation(data1, [1,1], E("distcorr")) < IdCor_estimation(data2, [1,1], E("distcorr"));
    res2("HOEFFDING") = IHoeffding_estimation(data1, [1,1], E("hoeffding")) < IHoeffding_estimation(data2, [1,1], E("hoeffding"));
    
    res2("SH_KNN") = HShannon_kNN_k_estimation(x, E("sh_knn")) + HShannon_kNN_k_estimation(y_res, E("sh_knn")) < HShannon_kNN_k_estimation(y, E("sh_knn")) + HShannon_kNN_k_estimation(x_res, E("sh_knn"));
    res2("SH_KNN_2") = HShannon_kNN_k_estimation(x, E("sh_knn_2")) + HShannon_kNN_k_estimation(y_res, E("sh_knn_2")) < HShannon_kNN_k_estimation(y, E("sh_knn_2")) + HShannon_kNN_k_estimation(x_res, E("sh_knn_2"));
    res2("SH_KNN_3") = HShannon_kNN_k_estimation(x, E("sh_knn_3")) + HShannon_kNN_k_estimation(y_res, E("sh_knn_3")) < HShannon_kNN_k_estimation(y, E("sh_knn_3")) + HShannon_kNN_k_estimation(x_res, E("sh_knn_3"));
    res2("SH_SPACING_V") = HShannon_spacing_V_estimation(x, E("sh_spacing_v")) + HShannon_spacing_V_estimation(y_res, E("sh_spacing_v")) < HShannon_spacing_V_estimation(y, E("sh_spacing_v")) + HShannon_spacing_V_estimation(x_res, E("sh_spacing_v"));
    res2("SH_MAXENT1") = HShannon_MaxEnt1_estimation(x, E("sh_maxent1")) + HShannon_MaxEnt1_estimation(y_res, E("sh_maxent1")) < HShannon_MaxEnt1_estimation(y, E("sh_maxent1")) + HShannon_MaxEnt1_estimation(x_res, E("sh_maxent1"));
    res2("SH_MAXENT2") = HShannon_MaxEnt2_estimation(x, E("sh_maxent2")) + HShannon_MaxEnt2_estimation(y_res, E("sh_maxent2")) < HShannon_MaxEnt2_estimation(y, E("sh_maxent2")) + HShannon_MaxEnt2_estimation(x_res, E("sh_maxent2"));
end


function dist_res = test_distribution(x_offset, x_sigma, eps_offset, eps_i_factor, x_dist, eps_dist, linear, E)
    success_count_hsic = 0;
    success_count_hsic_ic = 0;
    success_count_hsic_ic2 = 0;
    success_count_distcov = 0;
    success_count_distcorr = 0;
    success_count_hoeffding = 0;
    success_count_sh_knn = 0;
    success_count_sh_knn_2 = 0;
    success_count_sh_knn_3 = 0;
    success_count_sh_spacing_v = 0;
    success_count_sh_maxent1 = 0;
    success_count_sh_maxent2 = 0;

    success_count_hsic_s = 0;
    success_count_hsic_ic_s = 0;
    success_count_hsic_ic2_s = 0;
    success_count_distcov_s = 0;
    success_count_distcorr_s = 0;
    success_count_hoeffding_s = 0;
    success_count_sh_knn_s = 0;
    success_count_sh_knn_2_s = 0;
    success_count_sh_knn_3_s = 0;
    success_count_sh_spacing_v_s = 0;
    success_count_sh_maxent1_s = 0;
    success_count_sh_maxent2_s = 0;

    for test = 1:100
        x = x_dist(1, 1000, x_offset, x_sigma); %x_offset != 0 if uniform noise
        eps = eps_dist(1, 1000, eps_offset, eps_i_factor);
        if linear == 0
            x = x.^3;
        end
        y = x + eps;
        
        [res1, res2] = inference(x, y, E);

        if res2('HSIC') == 1
            success_count_hsic = success_count_hsic + 1;
        end
        if res2('HSIC_IC') == 1
            success_count_hsic_ic = success_count_hsic_ic + 1;
        end 
        if res2('HSIC_IC2') == 1
            success_count_hsic_ic2 = success_count_hsic_ic2 + 1;
        end
        if res2('DISTCOV') == 1
            success_count_distcov = success_count_distcov + 1;
        end
        if res2('DISTCORR') == 1
            success_count_distcorr = success_count_distcorr + 1;
        end
        if res2('HOEFFDING') == 1
            success_count_hoeffding = success_count_hoeffding + 1;
        end
        if res2('SH_KNN') == 1
            success_count_sh_knn = success_count_sh_knn + 1;
        end
        if res2('SH_KNN_2') == 1
            success_count_sh_knn_2 = success_count_sh_knn_2 + 1;
        end
        if res2('SH_KNN_3') == 1
            success_count_sh_knn_3 = success_count_sh_knn_3 + 1;
        end
        if res2('SH_SPACING_V') == 1
            success_count_sh_spacing_v = success_count_sh_spacing_v + 1;
        end
        if res2('SH_MAXENT1') == 1
            success_count_sh_maxent1 = success_count_sh_maxent1 + 1;
        end
        if res2('SH_MAXENT2') == 1
            success_count_sh_maxent2 = success_count_sh_maxent2 + 1;
        end

        if res1('HSIC_S') == 1
            success_count_hsic_s = success_count_hsic_s + 1;
        end
        if res1('HSIC_IC_S') == 1
            success_count_hsic_ic_s = success_count_hsic_ic_s + 1;
        end
        if res1('HSIC_IC2_S') == 1
            success_count_hsic_ic2_s = success_count_hsic_ic2_s + 1;
        end
        if res1('DISTCOV_S') == 1
            success_count_distcov_s = success_count_distcov_s + 1;
        end
        if res1('DISTCORR_S') == 1
            success_count_distcorr_s = success_count_distcorr_s + 1;
        end
        if res1('HOEFFDING_S') == 1
            success_count_hoeffding_s = success_count_hoeffding_s + 1;
        end
        if res1('SH_KNN_S') == 1
            success_count_sh_knn_s = success_count_sh_knn_s + 1;
        end
        if res1('SH_KNN_2_S') == 1
            success_count_sh_knn_2_s = success_count_sh_knn_2_s + 1;
        end
        if res1('SH_KNN_3_S') == 1
            success_count_sh_knn_3_s = success_count_sh_knn_3_s + 1;
        end
        if res1('SH_SPACING_V_S') == 1
            success_count_sh_spacing_v_s = success_count_sh_spacing_v_s + 1;
        end
        if res1('SH_MAXENT1_S') == 1
            success_count_sh_maxent1_s = success_count_sh_maxent1_s + 1;
        end
        if res1('SH_MAXENT2_S') == 1
            success_count_sh_maxent2_s = success_count_sh_maxent2_s + 1;
        end
    end

    dist_res = containers.Map;

    dist_res('HSIC') = success_count_hsic / 100;
    dist_res('HSIC_IC') = success_count_hsic_ic / 100;
    dist_res('HSIC_IC2') = success_count_hsic_ic2 / 100;
    dist_res('DISTCOV') = success_count_distcov / 100;
    dist_res('DISTCORR') = success_count_distcorr / 100;
    dist_res('HOEFFDING') = success_count_hoeffding / 100;
    dist_res('SH_KNN') = success_count_sh_knn / 100;
    dist_res('SH_KNN_2') = success_count_sh_knn_2 / 100;
    dist_res('SH_KNN_3') = success_count_sh_knn_3 / 100;
    dist_res('SH_SPACING_V') = success_count_sh_spacing_v / 100;
    dist_res('SH_MAXENT1') = success_count_sh_maxent1 / 100;
    dist_res('SH_MAXENT2') = success_count_sh_maxent2 / 100;
    dist_res('HSIC_S') = success_count_hsic_s / 100;
    dist_res('HSIC_IC_S') = success_count_hsic_ic_s / 100;
    dist_res('HSIC_IC2_S') = success_count_hsic_ic2_s / 100;
    dist_res('DISTCOV_S') = success_count_distcov_s / 100;
    dist_res('DISTCORR_S') = success_count_distcorr_s / 100;
    dist_res('HOEFFDING_S') = success_count_hoeffding_s / 100;
    dist_res('SH_KNN_S') = success_count_sh_knn_s / 100;
    dist_res('SH_KNN_2_S') = success_count_sh_knn_2_s / 100;
    dist_res('SH_KNN_3_S') = success_count_sh_knn_3_s / 100;
    dist_res('SH_SPACING_V_S') = success_count_sh_spacing_v_s / 100;
    dist_res('SH_MAXENT1_S') = success_count_sh_maxent1_s / 100;
    dist_res('SH_MAXENT2_S') = success_count_sh_maxent2_s / 100;
end






function main()
    % Map of Estimators
    E = containers.Map;

    E("sh_knn") = HShannon_kNN_k_initialization(1);

    sh_knn_2 = HShannon_kNN_k_initialization(1);
    sh_knn_2.kNNmethod = 'knnsearch';
    sh_knn_2.NSmethod = 'kdtree';
    sh_knn_2.epsi = 0.1;
    E("sh_knn_2") = sh_knn_2;

    sh_knn_3 = HShannon_kNN_k_initialization(1);
    sh_knn_3.k = 5;
    E("sh_knn_3") = sh_knn_3;

    E("sh_spacing_v") = HShannon_spacing_V_initialization(1);
    E("sh_maxent1") = HShannon_MaxEnt1_initialization(1);
    E("sh_maxent2") = HShannon_MaxEnt2_initialization(1);

    % data = vertcat(x,y);
    % ind -> estimation(data, [1,1], ind);
    E("hsic_ic") = IHSIC_initialization(1);

    hsic_ic2 = IHSIC_initialization(1);
    hsic_ic2.eta = 0.01;
    E("hsic_ic2") = hsic_ic2;

    E("distcorr") = IdCor_initialization(1);
    E("distcov") = IdCov_initialization(1);
    E("hoeffding") = IHoeffding_initialization(1);

    p.sigx = -1;
    p.sigy = -1;

    E("p") = p;
    %[thresh,testStat] = hsic(x1,y1,0.05,p);
    
    x_means = -100:10:100;    
    eps_means = -100:10:100;    

    x_means = [0];
    eps_means = [0];

    final_res = containers.Map;
    
    normal = @normal_rng;
    uniform = @uniform_rng;
    laplace = @laprnd;
       
    for x_m = x_means
        for eps_m = eps_means
            fprintf("Loop: %d\n", x_m);
            temp_res = containers.Map;
            % test_distribution(x_offset, x_sigma, eps_offset, eps_i_factor, x_dist, eps_dist, linear, Estimators)
            temp_res("GAU")        = test_distribution(x_m + 0, 1, eps_m + 0, 1, normal,  normal,  1, E);
            temp_res("UNI")        = test_distribution(x_m + 1, 2, eps_m + 1, 2, uniform, uniform, 1, E);
            temp_res("LAP")        = test_distribution(x_m + 0, 1, eps_m + 0, 1, laplace, laplace, 1, E);
            temp_res("NLGAU")      = test_distribution(x_m + 0, 1, eps_m + 0, 1, normal,  normal,  0, E);
            temp_res("NLUNI")      = test_distribution(x_m + 1, 2, eps_m + 1, 2, uniform, uniform, 0, E);
            temp_res("NLLAP")      = test_distribution(x_m + 0, 1, eps_m + 0, 1, laplace, laplace, 0, E);
            temp_res("GAUxUNI")    = test_distribution(x_m + 0, 1, eps_m + 1, 2, normal,  uniform, 1, E);
            temp_res("GAUxLAP")    = test_distribution(x_m + 0, 1, eps_m + 0, 1, normal,  laplace, 1, E);
            temp_res("NL_GAUxUNI") = test_distribution(x_m + 0, 1, eps_m + 1, 2, normal,  uniform, 0, E);
            temp_res("NL_GAUxLAP") = test_distribution(x_m + 0, 1, eps_m + 0, 1, normal,  laplace, 0, E);
            temp_res("UNIxGAU")    = test_distribution(x_m + 1, 2, eps_m + 0, 1, uniform, normal,  1, E);
            temp_res("UNIxLAP")    = test_distribution(x_m + 1, 2, eps_m + 0, 1, uniform, laplace, 1, E);
            temp_res("NL_UNIxGAU") = test_distribution(x_m + 1, 2, eps_m + 0, 1, uniform, normal,  0, E);
            temp_res("NL_UNIxLAP") = test_distribution(x_m + 1, 2, eps_m + 0, 1, uniform, laplace, 0, E);
            temp_res("LAPxGAU")    = test_distribution(x_m + 0, 1, eps_m + 0, 1, laplace, normal,  1, E);
            temp_res("LAPxUNI")    = test_distribution(x_m + 0, 1, eps_m + 1, 2, laplace, uniform, 1, E);
            temp_res("NL_LAPxGAU") = test_distribution(x_m + 0, 1, eps_m + 0, 1, laplace, normal,  0, E);
            temp_res("NL_LAPxUNI") = test_distribution(x_m + 0, 1, eps_m + 1, 2, laplace, uniform, 0, E);
            
 
            key = string(x_m) + "/" + string(eps_m);
            final_res(key) = temp_res;
        end
    end
    fileID = fopen("Resit3CompleteRun", "w");
    fprintf(fileID, jsonencode(final_res));
end







function y  = normal_rng(m, n, offset, sigma)
   y = sigma .* randn(m, n) - offset;
end




function y = uniform_rng(m, n, offset, sigma)
   y = sigma .* rand(m, n) - offset;
end



function y  = laprnd(m, n, mu, sigma)
    %LAPRND generate i.i.d. laplacian random number drawn from laplacian distribution
    %   with mean mu and standard deviation sigma. 
    %   mu      : mean
    %   sigma   : standard deviation
    %   [m, n]  : the dimension of y.
    %   Default mu = 0, sigma = 1. 
    %   For more information, refer to
    %   http://en.wikipedia.org./wiki/Laplace_distribution
    %   Author  : Elvis Chen (bee33@sjtu.edu.cn)
    %   Date    : 01/19/07
    %Check inputs
    if nargin < 2
        error('At least two inputs are required');
    end
    if nargin == 2
        mu = 0; sigma = 1;
    end
    if nargin == 3
        sigma = 1;
    end
    % Generate Laplacian noise
    u = rand(m, n)-0.5;
    b = sigma / sqrt(2);
    y = mu - b * sign(u).* log(1- 2* abs(u));
end



function [H]=rbf_dot(patterns1,patterns2,deg);
    %Note : patterns are transposed for compatibility with C code.
    size1=size(patterns1);
    size2=size(patterns2);
    %new vectorised version
    G = sum((patterns1.*patterns1),2);
    H = sum((patterns2.*patterns2),2);

    Q = repmat(G,1,size2(1));
    R = repmat(H',size1(1),1);

    H = Q + R - 2*patterns1*patterns2';


    H=exp(-H/2/deg^2);
end

function testStat = hsicTestGamma(X,Y,alpha,params);
    m=size(X,1);
    %Set kernel size to median distance between points, if no kernel specified.
    %Use at most 100 points (since median is only a heuristic, and 100 points
    %is sufficient for a robust estimate).
    if params.sigx == -1
        size1=size(X,1);
        if size1>100
          Xmed = X(1:100,:);
          size1 = 100;
        else
          Xmed = X;
        end
        G = sum((Xmed.*Xmed),2);
        Q = repmat(G,1,size1);
        R = repmat(G',size1,1);
        dists = Q + R - 2*Xmed*Xmed';
        dists = dists-tril(dists);
        dists=reshape(dists,size1^2,1);
        params.sigx = sqrt(0.5*median(dists(dists>0)));  %rbf_dot has factor of two in kernel
    end

    if params.sigy == -1
        size1=size(Y,1);
        if size1>100
          Ymed = Y(1:100,:);
          size1 = 100;
        else
          Ymed = Y;
        end    
        G = sum((Ymed.*Ymed),2);
        Q = repmat(G,1,size1);
        R = repmat(G',size1,1);
        dists = Q + R - 2*Ymed*Ymed';
        dists = dists-tril(dists);
        dists=reshape(dists,size1^2,1);
        params.sigy = sqrt(0.5*median(dists(dists>0)));
    end



    bone = ones(m,1);
    H = eye(m)-1/m*ones(m,m);


    K = rbf_dot(X,X,params.sigx);
    L = rbf_dot(Y,Y,params.sigy);

    Kc = H*K*H; %Note: these are slightly biased estimates of centred Gram matrices
    Lc = H*L*H;
      
    %NOTE: we fit Gamma to testStat*m
    testStat = 1/m * sum(sum(Kc'.*Lc));    %%%% TEST STATISTIC: m*HSICb (under H1)
      
    varHSIC = (1/6 * Kc.*Lc).^2;
      
    varHSIC = 1/m/(m-1)* (  sum(sum(varHSIC)) - sum(diag(varHSIC))  ); 
    %second subtracted term is bias correction
      
    varHSIC = 72*(m-4)*(m-5)/m/(m-1)/(m-2)/(m-3)  *  varHSIC;  %variance under H0


    K = K-diag(diag(K));
    L = L-diag(diag(L));

    muX = 1/m/(m-1)*bone'*(K*bone);
    muY = 1/m/(m-1)*bone'*(L*bone);
      
    mHSIC  = 1/m * ( 1 +muX*muY  - muX - muY )         ;  %mean under H0


    al = mHSIC^2 / varHSIC;
    bet = varHSIC*m / mHSIC;   %NOTE: threshold for hsicArr*m

    thresh = icdf('gam',1-alpha,al,bet);    %%%% TEST THRESHOLD
end




