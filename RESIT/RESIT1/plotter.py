import matplotlib.pyplot as plt
import matplotlib
from matplotlib.path import Path
import matplotlib.patches as patches
import json
import numpy as np
import pandas as pd
from matplotlib.pyplot import gca
from scipy.interpolate import make_interp_spline

def load_data(path):
    data = None
    with open(path) as file_content:
        data = json.load(file_content)
    return data

def convert_data(data):
    result = {}
    for k, v in data.items():
        for k2, v2 in v.items():
            if k2 in result:
                result[k2] = {**result[k2], **{k: v2}}
            else:
                result[k2] = {k: v2}
    return result


def create_plot2(name, data, x_ticks, y_ticks, legend=True):
    color = ["#dd7e2a", "#2aa4dd", "#95dd2a", "#1c5621", "#874edb", "#db4e6f", "#efed64", "#177c19", "#2830d3", "#ef64e4",
             "#332f2f", "#04cdf9"]
    ls = ["-", "-", "-", "-", "-", "-", "--", "--", "--", "--", "--", "--"]
    count = 0
    for k, v in data.items():
        a, b = np.array_split(v, 2, axis=1)
        #print(a)
        
        # Smoothing
        #X_Y_Spline = make_interp_spline(a.flatten(), b.flatten())
        #a = np.linspace(a.min(), a.max(), 500)
        #b = X_Y_Spline(a)

        #coefs = np.polyfit(a, b, 20)
        #b = np.polyval(coefs, a)

        plt.plot(a, b, color=color[count], label=k, alpha=0.75, linestyle=ls[count])
        count += 1
    plt.ylim(-0.05, 1.05)
    plt.axhline(0.5, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(1.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.axhline(0.0, linestyle=(0, (1, 5)), color='grey', alpha=0.75, label="_")
    plt.xticks(x_ticks)
    plt.yticks(y_ticks)
    plt.grid(axis='both', alpha=0.3)
    plt.ylabel("Accuracy")
    plt.xlabel("Standard deviation factor for noise")
    if legend:
        plt.legend(
            bbox_to_anchor=(1, 1),
            loc="upper left",
        )
    plt.savefig(name + ".png", format="png", bbox_inches='tight', dpi=300)
    plt.clf()
    plt.cla()
    plt.close()

def main():
    path = "results/" + "Resit1CompleteRun"
    data = load_data(path)

    factors2 = np.arange(1, 101, 1)
    factors1 = np.arange(0.01, 1.00, 0.01)  # 1 / factors2
    np.append(factors1, 1)

    combinations = []  # e.g., GAU, GAUxUNI, etc
    for k, _ in data["1"].items():
        combinations.append(k)

    # np.array_split(z, 2, axis=1)

    for comb in combinations:
        HSIC, HSIC_IC, HSIC_IC2, DISTCOV, DISTCORR, HOEFFDING, SH_KNN, SH_KNN_2, SH_KNN_3, SH_MAXENT1, SH_MAXENT2, SH_SPACING_V = [], [], [], [], [], [], [], [], [], [], [], []
        for i in factors1:
            temp = data[str(np.round(i, 2))]
            HSIC.append([i, temp[comb]["HSIC"]])
            HSIC_IC.append([i, temp[comb]["HSIC_IC"]])
            HSIC_IC2.append([i, temp[comb]["HSIC_IC2"]])
            DISTCOV.append([i, temp[comb]["DISTCOV"]])
            DISTCORR.append([i, temp[comb]["DISTCORR"]])
            HOEFFDING.append([i, temp[comb]["HOEFFDING"]])
            SH_KNN.append([i, temp[comb]["SH_KNN"]])
            SH_KNN_2.append([i, temp[comb]["SH_KNN_2"]])
            SH_KNN_3.append([i, temp[comb]["SH_KNN_3"]])
            SH_MAXENT1.append([i, temp[comb]["SH_MAXENT1"]])
            SH_MAXENT2.append([i, temp[comb]["SH_MAXENT2"]])
            SH_SPACING_V.append([i, temp[comb]["SH_SPACING_V"]])

        #print(HSIC)
        #print(np.array(HSIC))

        plot_data = {
            'HSIC': np.array(HSIC),
            'HSIC_IC': np.array(HSIC_IC),
            'HSIC_IC2': np.array(HSIC_IC2),
            'DISTCOV': np.array(DISTCOV),
            'DISTCORR': np.array(DISTCORR),
            'HOEFFDING': np.array(HOEFFDING),
            'SH_KNN': np.array(SH_KNN),
            'SH_KNN_2': np.array(SH_KNN_2),
            'SH_KNN_3': np.array(SH_KNN_3),
            'SH_MAXENT1': np.array(SH_MAXENT1),
            'SH_MAXENT2': np.array(SH_MAXENT2),
            'SH_SPACING_V': np.array(SH_SPACING_V),
        }

        create_plot2("plots/1" + comb, plot_data, np.arange(0, 1.1, 0.1), np.arange(0, 1.1, 0.1), legend=False)
        #continue

        HSIC, HSIC_IC, HSIC_IC2, DISTCOV, DISTCORR, HOEFFDING, SH_KNN, SH_KNN_2, SH_KNN_3, SH_MAXENT1, SH_MAXENT2, SH_SPACING_V = [], [], [], [], [], [], [], [], [], [], [], []
        for i in factors2:
            temp = data[str(i)]
            HSIC.append([i, temp[comb]["HSIC"]])
            HSIC_IC.append([i, temp[comb]["HSIC_IC"]])
            HSIC_IC2.append([i, temp[comb]["HSIC_IC2"]])
            DISTCOV.append([i, temp[comb]["DISTCOV"]])
            DISTCORR.append([i, temp[comb]["DISTCORR"]])
            HOEFFDING.append([i, temp[comb]["HOEFFDING"]])
            SH_KNN.append([i, temp[comb]["SH_KNN"]])
            SH_KNN_2.append([i, temp[comb]["SH_KNN_2"]])
            SH_KNN_3.append([i, temp[comb]["SH_KNN_3"]])
            SH_MAXENT1.append([i, temp[comb]["SH_MAXENT1"]])
            SH_MAXENT2.append([i, temp[comb]["SH_MAXENT2"]])
            SH_SPACING_V.append([i, temp[comb]["SH_SPACING_V"]])

        plot_data = {
            'HSIC': np.array(HSIC),
            'HSIC_IC': np.array(HSIC_IC),
            'HSIC_IC2': np.array(HSIC_IC2),
            'DISTCOV': np.array(DISTCOV),
            'DISTCORR': np.array(DISTCORR),
            'HOEFFDING': np.array(HOEFFDING),
            'SH_KNN': np.array(SH_KNN),
            'SH_KNN_2': np.array(SH_KNN_2),
            'SH_KNN_3': np.array(SH_KNN_3),
            'SH_MAXENT1': np.array(SH_MAXENT1),
            'SH_MAXENT2': np.array(SH_MAXENT2),
            'SH_SPACING_V': np.array(SH_SPACING_V),
        }

        create_plot2("plots/2" + comb, plot_data, np.arange(0, 101, 10), np.arange(0, 1.1, 0.1))


        HSIC, HSIC_IC, HSIC_IC2, DISTCOV, DISTCORR, HOEFFDING, SH_KNN, SH_KNN_2, SH_KNN_3, SH_MAXENT1, SH_MAXENT2, SH_SPACING_V = [], [], [], [], [], [], [], [], [], [], [], []
        for i in factors1:
            temp = data[str(np.round(i, 2))]
            HSIC.append([i, temp[comb]["HSIC_S"]])
            HSIC_IC.append([i, temp[comb]["HSIC_IC_S"]])
            HSIC_IC2.append([i, temp[comb]["HSIC_IC2_S"]])
            DISTCOV.append([i, temp[comb]["DISTCOV_S"]])
            DISTCORR.append([i, temp[comb]["DISTCORR_S"]])
            HOEFFDING.append([i, temp[comb]["HOEFFDING_S"]])
            SH_KNN.append([i, temp[comb]["SH_KNN_S"]])
            SH_KNN_2.append([i, temp[comb]["SH_KNN_2_S"]])
            SH_KNN_3.append([i, temp[comb]["SH_KNN_3_S"]])
            SH_MAXENT1.append([i, temp[comb]["SH_MAXENT1_S"]])
            SH_MAXENT2.append([i, temp[comb]["SH_MAXENT2_S"]])
            SH_SPACING_V.append([i, temp[comb]["SH_SPACING_V_S"]])

        #print(HSIC)
        #print(np.array(HSIC))

        plot_data = {
            'HSIC_S': np.array(HSIC),
            'HSIC_IC_S': np.array(HSIC_IC),
            'HSIC_IC2_S': np.array(HSIC_IC2),
            'DISTCOV_S': np.array(DISTCOV),
            'DISTCORR_S': np.array(DISTCORR),
            'HOEFFDING_S': np.array(HOEFFDING),
            'SH_KNN_S': np.array(SH_KNN),
            'SH_KNN_2_S': np.array(SH_KNN_2),
            'SH_KNN_3_S': np.array(SH_KNN_3),
            'SH_MAXENT1_S': np.array(SH_MAXENT1),
            'SH_MAXENT2_S': np.array(SH_MAXENT2),
            'SH_SPACING_V_S': np.array(SH_SPACING_V),
        }

        create_plot2("plots/1" + comb + "_split", plot_data, np.arange(0, 1.1, 0.1), np.arange(0, 1.1, 0.1), legend=False)
        #continue

        HSIC, HSIC_IC, HSIC_IC2, DISTCOV, DISTCORR, HOEFFDING, SH_KNN, SH_KNN_2, SH_KNN_3, SH_MAXENT1, SH_MAXENT2, SH_SPACING_V = [], [], [], [], [], [], [], [], [], [], [], []
        for i in factors2:
            temp = data[str(i)]
            HSIC.append([i, temp[comb]["HSIC_S"]])
            HSIC_IC.append([i, temp[comb]["HSIC_IC_S"]])
            HSIC_IC2.append([i, temp[comb]["HSIC_IC2_S"]])
            DISTCOV.append([i, temp[comb]["DISTCOV_S"]])
            DISTCORR.append([i, temp[comb]["DISTCORR_S"]])
            HOEFFDING.append([i, temp[comb]["HOEFFDING_S"]])
            SH_KNN.append([i, temp[comb]["SH_KNN_S"]])
            SH_KNN_2.append([i, temp[comb]["SH_KNN_2_S"]])
            SH_KNN_3.append([i, temp[comb]["SH_KNN_3_S"]])
            SH_MAXENT1.append([i, temp[comb]["SH_MAXENT1_S"]])
            SH_MAXENT2.append([i, temp[comb]["SH_MAXENT2_S"]])
            SH_SPACING_V.append([i, temp[comb]["SH_SPACING_V_S"]])

        plot_data = {
            'HSIC_S': np.array(HSIC),
            'HSIC_IC_S': np.array(HSIC_IC),
            'HSIC_IC2_S': np.array(HSIC_IC2),
            'DISTCOV_S': np.array(DISTCOV),
            'DISTCORR_S': np.array(DISTCORR),
            'HOEFFDING_S': np.array(HOEFFDING),
            'SH_KNN_S': np.array(SH_KNN),
            'SH_KNN_2_S': np.array(SH_KNN_2),
            'SH_KNN_3_S': np.array(SH_KNN_3),
            'SH_MAXENT1_S': np.array(SH_MAXENT1),
            'SH_MAXENT2_S': np.array(SH_MAXENT2),
            'SH_SPACING_V_S': np.array(SH_SPACING_V),
        }

        create_plot2("plots/2" + comb + "_split", plot_data, np.arange(0, 101, 10), np.arange(0, 1.1, 0.1))





if __name__ == "__main__":
    main()
    
    
    
    
