# The Effect of Noise Level on Causal Identification with Additive Noise Models

## Content
This repository contains code, results (in JSON format), and plots (.png) of the thesis text `The Effect of Noise Level on Causal Identification with Additive Noise Models`.

The structure is as follows:
RESIT contains the three different experiments RESIT1, RESIT2 and RESIT3. PARK contains the 4th experiment.
In each experiment folder you find code files used for the experiments (.m for RESIT and .py for PARK),
a `results` folder containing the results formatted as JSON (see below), a `plots` folder containing plots of all the results
and a `plotter.py` file used for creating the plots.

## JSON
### RESIT
* {key1:{key2:{key3:value}}}
* key1: i-factor
* key2: combination (e.g., GAUxLAP: X is drawn from gaussian distribution and N_y is drawn from Laplace distribution)
* key3: Estimator. An estimator subscripted with _S signifies the decoupled version.
* value: the accuracy value obtained for the particular i-factor + combination + estimator.

### PARK
* {key1:{key2:value}}
* key1: i-factor
* key2: combination (e.g., GAUxLAP: X is drawn from gaussian distribution and N_y is drawn from Laplace distribution)
* value: the accuracy value obtained for the particular i-factor + combination.
